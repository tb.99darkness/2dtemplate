using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    [CreateAssetMenu(fileName = "GameEvent", menuName = "ScriptableObjects/GameEvent", order = 1)]
    public class GameEvent : ScriptableObject
    {
        private List<IGameEvent> _listeners = new List<IGameEvent>();

        public void AddListener(IGameEvent listener)
        {
            _listeners.Add(listener);
        }

        public void RemoveListener(IGameEvent listener)
        {
            _listeners.Add(listener);
        }

        public void RaiseEvent()
        {
            int count = _listeners.Count;
            for (int i = 0; i < count; i++)
            {
                _listeners[i].Raise();
            }
        }
    }
}
