using System.Collections;
using System.Collections.Generic;
using Lean;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Runtime
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject _playerPrefab;
        [SerializeField]
        private Transform _playerInstantiatePositon;
        [SerializeField]
        private List<WaveConfig> _waveConfigs = new List<WaveConfig>();
        [SerializeField]
        private List<Transform> _spawnPoint = new List<Transform>();
        [ReadOnly]
        [ShowInInspector]
        private int _currentWave = 0;

        private GameObject _player;

        private void Awake()
        {
            _player = GameObject.Instantiate(_playerPrefab, _playerInstantiatePositon.position, Quaternion.identity);
        }

        private void Start()
        {
            SpawnEnemy();
        }


        private void SpawnEnemy()
        {
            WaveConfig wave = _waveConfigs[_currentWave];
            int count = wave.turnConfigs.Count;
            int pointIndex = 0;
            AIController aIController;
            GameObject enemy;
            for (int i = 0; i < count; i++)
            {
                TurnConfig turn = wave.turnConfigs[i];
                for (int j = 0; j < turn.number; j++)
                {
                    pointIndex = Random.Range(0, _spawnPoint.Count - 1);
                    enemy = LeanPool.Spawn(turn.enemy, _spawnPoint[pointIndex].position, Quaternion.identity);
                    if (enemy.TryGetComponent<AIController>(out aIController))
                    {
                        aIController._target = _player.transform;
                    }
                }

            }
            _currentWave++;
        }
    }
}
