﻿namespace Game.Runtime
{
    public interface IGameEvent
    {
        void Raise();
    }
}