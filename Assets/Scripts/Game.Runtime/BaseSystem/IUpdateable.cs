using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    public interface IUpdateable
    {
        void CustomUpdate();

        void CustomFixedUpdate();
    }
}
