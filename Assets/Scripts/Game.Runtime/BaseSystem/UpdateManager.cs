using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    public class UpdateManager : MonoBehaviour
    {
        private static List<IUpdateable> updateCallBack = new List<IUpdateable>();
        private static List<IUpdateable> fixedUpdateCallBack = new List<IUpdateable>();


        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            for (int i = 0; i < fixedUpdateCallBack.Count; i++)
            {
                fixedUpdateCallBack[i].CustomFixedUpdate();
            }
        }


        void Update()
        {
            for (int i = 0; i < updateCallBack.Count; i++)
            {
                updateCallBack[i].CustomUpdate();
            }
        }


        public static void RegisterUpdate(IUpdateable register)
        {
            AddCallBackTolist(ref updateCallBack, register);
        }

        public static void UnregisterUpdate(IUpdateable register)
        {
            RemoveCallBackTolist(ref updateCallBack, register);
        }

        public static void RegisterFixedUpdate(IUpdateable register)
        {
            AddCallBackTolist(ref fixedUpdateCallBack, register);
        }

        public static void UnregisterFixedUpdate(IUpdateable register)
        {
            RemoveCallBackTolist(ref fixedUpdateCallBack, register);
        }

        private static void AddCallBackTolist(ref List<IUpdateable> updateables, IUpdateable register)
        {
            if (!updateables.Contains(register))
            {
                updateables.Add(register);
            }
        }

        private static void RemoveCallBackTolist(ref List<IUpdateable> updateables, IUpdateable register)
        {
            if (updateables.Contains(register))
            {
                updateables.Remove(register);
            }
        }


    }
}
