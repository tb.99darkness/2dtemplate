﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Runtime
{
    public class AIController : ActionController
    {
        /// <summary>
        ///   phải lấy từ manager
        /// </summary>
        public Transform _target;
        private HpController _playerHpController;
        private Vector2 _direction;
        [ShowInInspector]
        private AIState _currentState = AIState.CHASE;
        protected override void CustomOnEnable()
        {
            startMove?.Invoke();
            UpdateManager.RegisterFixedUpdate(this);
        }

        private void Start()
        {
            _target.TryGetComponent<HpController>(out _playerHpController);
        }

        protected override void CustomOnDisable()
        {
            cancelMove?.Invoke();
            UpdateManager.UnregisterFixedUpdate(this);
        }

        public override void CustomFixedUpdate()
        {
            AIControl();
        }

        private void AIControl()
        {
            switch (_currentState)
            {
                case AIState.CHASE:
                    ChaseTarget();
                    break;
                case AIState.ATTACK1:
                    Attack1();
                    break;
                case AIState.ATTACK2:
                    break;
                case AIState.ATTACK3:
                    break;
                case AIState.DEAD:
                    break;
                default:
                    break;
            }
        }

        private void ChaseTarget()
        {
            _direction = _target.position - transform.position;
            /// sẽ đổi thành khoảng cách của skill
            if (Mathf.Abs(_direction.x) < 5 && !_playerHpController.isDead)
            {
                SetState(AIState.ATTACK1);
                cancelMove?.Invoke();
                return;
            }
            moveEvent?.Invoke(_direction.normalized);
        }

        private void Attack1()
        {
            /// sẽ đổi thành khoảng cách của skill
            if (GetTargetDistance() > 5 || _playerHpController.isDead)
            {
                SetState(AIState.CHASE);
                return;
            }

            attackEvent?.Invoke();
        }


        private void SetState(AIState state)
        {
            switch (state)
            {
                case AIState.CHASE:
                    startMove?.Invoke();
                    _currentState = state;
                    break;
                case AIState.ATTACK1:
                    _currentState = state;
                    break;
                case AIState.ATTACK2:
                    break;
                case AIState.ATTACK3:
                    break;
                case AIState.DEAD:
                    break;
                default:
                    break;
            }
        }

        private float GetTargetDistance()
        {
            _direction = _target.position - transform.position;
            return Mathf.Abs(_direction.x);
        }



        public enum AIState
        {
            CHASE,
            ATTACK1,
            ATTACK2,
            ATTACK3,
            DEAD
        }
    }
}
