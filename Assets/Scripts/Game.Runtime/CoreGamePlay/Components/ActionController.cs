using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Game.Runtime
{
    public class ActionController : MonoBehaviour, IUpdateable, IGameEvent
    {
        [HideInInspector]
        public UnityEvent<Vector2> moveEvent;
        [HideInInspector]
        public UnityEvent attackEvent,startMove,cancelMove,onSwichPreviousChar,onSwichNextChar;
        [SerializeField]
        private GameEvent _onHeroDead;
        private PlayerInput _playerInput;
        private bool _isMoving;
        private Vector2 _moveDirect;
        private Vector2 _lastMove;
        private InputAction _moveInputAction;


        private void OnEnable()
        {
            _onHeroDead.AddListener(this);
            CustomOnEnable();
        }

        protected virtual void CustomOnEnable()
        {
            UpdateManager.RegisterFixedUpdate(this);
            _playerInput = new PlayerInput();
            _playerInput.Enable();
            _moveInputAction = _playerInput.GamePlay.Move;
            _playerInput.GamePlay.PreviousChar.performed += ctx => SwichPreviousChar();
            _playerInput.GamePlay.NextChar.performed += ctx => SwichNextChar();
            _playerInput.GamePlay.Attack.performed += ctx => Attack();
            _playerInput.GamePlay.Move.performed += ctx => StartMove();
            _playerInput.GamePlay.Move.canceled += ctx => CancelMove();
        }


        private void OnDisable()
        {
            CustomOnDisable();
        }


        protected virtual void CustomOnDisable()
        {
            UpdateManager.UnregisterFixedUpdate(this);
            _playerInput.Disable();
            _playerInput.GamePlay.PreviousChar.performed -= ctx => SwichPreviousChar();
            _playerInput.GamePlay.NextChar.performed -= ctx => SwichNextChar();
            _playerInput.GamePlay.Attack.performed -= ctx => Attack();
            _playerInput.GamePlay.Move.performed -= ctx => StartMove();
            _playerInput.GamePlay.Move.canceled -= ctx => CancelMove();
        }


        public void CustomUpdate()
        {

        }

        public virtual void CustomFixedUpdate()
        {
            ManualControl();
        }

        private void ManualControl()
        {
            if (_playerInput.asset.enabled)
            {
                if (_isMoving)
                {
                    _moveDirect = _moveInputAction.ReadValue<Vector2>();
                    _lastMove = _moveDirect;
                    moveEvent?.Invoke(_lastMove);
                }
                else
                {
                    if (_lastMove != CommonConstant.Vector2Zero)
                    {
                        _lastMove = CommonConstant.Vector2Zero;
                        moveEvent?.Invoke(_lastMove);
                    }
                }
            }
        }


        private void SwichPreviousChar()
        {
            onSwichPreviousChar?.Invoke();
        }


        private void SwichNextChar()
        {
            onSwichNextChar?.Invoke();
        }

        private void Attack()
        {
            attackEvent?.Invoke();
        }


        private void StartMove()
        {
            startMove?.Invoke();
            _isMoving = true;
        }

        private void CancelMove()
        {
            cancelMove?.Invoke();
            _isMoving = false;
        }

        public void RegisterListener<T>(T register) where T : ComponentController
        {
            Type type = register.GetType();
            Type[] interfacesArray =  type.GetInterfaces();
            if (interfacesArray.Length > 0)
            {
                for (int i = 0; i < interfacesArray.Length; i++)
                {
                    RegistListenerByInterface(interfacesArray[i], register);
                }
            }
        }

        public void UnregisterListener<T>(T register) where T : ComponentController
        {
            Type type = register.GetType();
            Type[] interfacesArray =  type.GetInterfaces();
            if (interfacesArray.Length > 0)
            {
                for (int i = 0; i < interfacesArray.Length; i++)
                {
                    UnregistListenerByInterface(interfacesArray[i], register);
                }
            }
        }


        private void RegistListenerByInterface(Type type, ComponentController component)
        {
            switch (type.Name)
            {
                case "IAttackListener":
                    var attackListener = component as IAttackListener;
                    attackEvent.AddListener(attackListener.OnAttack);
                    break;
                case "IMoveListener":
                    var moveListener = component as IMoveListener;
                    moveEvent.AddListener(moveListener.OnMove);
                    startMove.AddListener(moveListener.OnStartMove);
                    cancelMove.AddListener(moveListener.OnCacelMove);
                    break;
                case "ISwichListener":
                    var swichListener = component as ISwichListener;
                    onSwichNextChar.AddListener(swichListener.OnSwichNextCharacter);
                    onSwichPreviousChar.AddListener(swichListener.OnSwichPreviousCharacter);
                    break;
            }
        }


        private void UnregistListenerByInterface(Type type, ComponentController component)
        {
            switch (type.Name)
            {
                case "IAttackListener":
                    var attackListener = component as IAttackListener;
                    attackEvent.RemoveListener(attackListener.OnAttack);
                    break;
                case "IMoveListener":
                    var moveListener = component as IMoveListener;
                    moveEvent.RemoveListener(moveListener.OnMove);
                    startMove.RemoveListener(moveListener.OnStartMove);
                    cancelMove.RemoveListener(moveListener.OnCacelMove);
                    break;
                case "ISwichListener":
                    var swichListener = component as ISwichListener;
                    onSwichNextChar.RemoveListener(swichListener.OnSwichNextCharacter);
                    onSwichPreviousChar.RemoveListener(swichListener.OnSwichPreviousCharacter);
                    break;
            }
        }

        public virtual void Raise()
        {
            this.enabled = false;
        }
    }
}
