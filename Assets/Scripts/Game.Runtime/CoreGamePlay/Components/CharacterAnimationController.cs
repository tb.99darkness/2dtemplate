using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Runtime
{
    public class CharacterAnimationController : ComponentController, IMoveListener, IAttackListener
    {
        [SerializeField]
        public Character character;
        private AnimationHash _animationHash;

        protected override void CustomOnEnable()
        {
            base.CustomOnEnable();
            _animationHash.attack_1Hash = Animator.StringToHash(AnimationConstant.Attack_1);
            _animationHash.runHash = Animator.StringToHash(AnimationConstant.Is_moving);
            _animationHash.idleHash = Animator.StringToHash(AnimationConstant.Idle);
            _animationHash.deadHash = Animator.StringToHash(AnimationConstant.Dead);
        }


        public void OnCacelMove()
        {
            character._animator.SetBool(_animationHash.runHash, false);
        }

        public void OnMove(Vector2 vector)
        {

        }

        public void OnStartMove()
        {
            character._animator.SetBool(_animationHash.runHash, true);
        }

        public void OnAttack()
        {
            character._animator.SetTrigger(_animationHash.attack_1Hash);
        }

        public void OnDead()
        {
            character._animator.SetTrigger(_animationHash.deadHash);
        }

    }

    struct AnimationHash
    {
        public int runHash;
        public int idleHash;
        public int attack_1Hash;
        public int deadHash;
    }
}
