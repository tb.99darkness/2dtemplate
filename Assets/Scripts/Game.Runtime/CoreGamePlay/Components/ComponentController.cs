using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    [RequireComponent(typeof(ActionController))]
    public class ComponentController : MonoBehaviour
    {
        protected ActionController inputController;

        private void OnEnable()
        {
            CustomOnEnable();
        }
        private void OnDisable()
        {
            UnregisterInput();
        }

        protected virtual void CustomOnEnable()
        {
            if (gameObject.TryGetComponent<ActionController>(out inputController))
            {
                inputController.RegisterListener(this);
            }
        }

        private void UnregisterInput()
        {
            if (inputController != null)
            {
                inputController.UnregisterListener(this);
            }
        }
    }
}
