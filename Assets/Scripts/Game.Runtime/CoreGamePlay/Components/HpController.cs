using System.Collections;
using System.Collections.Generic;
using Kryz.CharacterStats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Runtime
{
    [RequireComponent(typeof(CharacterAnimationController))]
    [RequireComponent(typeof(Collider2D))]
    public class HpController : MonoBehaviour, IGameEvent
    {
        [SerializeField]
        private CharacterStat _hp;
        [ShowInInspector]
        private float _currentHp;
        [SerializeField]
        private CharacterAnimationController _heroAnimation;
        [SerializeField]
        private Collider2D _collider2D;
        [SerializeField]
        private Rigidbody2D _rigidbody;
        [SerializeField]
        private GameEvent _onHeroDead;
        public bool isDead = false;

        private void OnEnable()
        {
        }

        public void Modifi(float damage)
        {
            StatModifier modifier = new StatModifier(damage ,StatModType.Flat);
            _hp.AddModifier(modifier);
            _currentHp = _hp.Value;
            CheckDead();
        }

        private void CheckDead()
        {
            if (_currentHp <= 0)
            {
                isDead = true;
                _heroAnimation.OnDead();
                _collider2D.enabled = false;
                _rigidbody.isKinematic = true;
                _onHeroDead.RaiseEvent();
            }
        }

        public void Raise()
        {

        }
    }
}
