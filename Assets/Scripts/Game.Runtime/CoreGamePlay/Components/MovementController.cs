using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Kryz.CharacterStats;

namespace Game.Runtime
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class MovementController : ComponentController, IMoveListener
    {
        [Required]
        [SerializeField]
        private Rigidbody2D _rigidbody;
        public CharacterStat speed;
        public void OnCacelMove()
        {

        }

        public void OnMove(Vector2 vector)
        {
            if (vector.x < 0)
            {
                transform.localRotation = Quaternion.Euler(0, 180, 0);
            }
            else if (vector.x > 0)
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
            }
            _rigidbody.MovePosition(_rigidbody.position + vector * Time.deltaTime * speed.Value);
        }

        public void OnStartMove()
        {

        }
    }

}

