using System.Collections;
using System.Collections.Generic;
using Kryz.CharacterStats;
using UnityEngine;

namespace Game.Runtime
{
    public class SkillController : ComponentController, IAttackListener
    {
        [SerializeField]
        public Character _currentCharacter;
        [SerializeField]
        private CharacterStat _damage;
        [SerializeField]
        private Transform _castPos;
        [SerializeField]
        private LayerMask _layerMask;
        [SerializeField]

        public void OnAttack()
        {
            if (_currentCharacter._skill._lastCast > Time.time - _currentCharacter._skill._delayTime)
            {
                return;
            }
            for (int i = 0; i < _currentCharacter._skill._bulletNumber; i++)
            {
                StartCoroutine(_currentCharacter._skill.Excute(_castPos.rotation, _castPos.position, i, _damage.Value, _layerMask));
            }
            _currentCharacter._skill._lastCast = Time.time;
        }
    }
}

