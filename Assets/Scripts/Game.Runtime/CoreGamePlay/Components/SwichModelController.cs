using System.Collections;
using System.Collections.Generic;
using Lean;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Runtime
{
    public class SwichModelController : ComponentController, ISwichListener
    {
        [SerializeField]
        private ListModelConfig _listModelConfig;
        [SerializeField]
        private Transform _modelHolder;
        [ReadOnly]
        [ShowInInspector]
        private Character _currentCharacter;
        [SerializeField]
        private CharacterAnimationController _heroAnimationController;
        [SerializeField]
        private SkillController _skillController;

        private void Awake()
        {
            _currentCharacter = LeanPool.Spawn(_listModelConfig.GetCurrentCharacter(), Vector3.zero, Quaternion.identity, _modelHolder);
        }

        protected override void CustomOnEnable()
        {
            base.CustomOnEnable();
            if (_heroAnimationController != null)
            {
                _heroAnimationController.character = this._currentCharacter;
            }
            if (_skillController != null)
            {
                _skillController._currentCharacter = this._currentCharacter;
            }
        }

        public void OnSwichNextCharacter()
        {
            LeanPool.Despawn(_currentCharacter);
            _currentCharacter = LeanPool.Spawn(_listModelConfig.GetNextCharacter(), Vector3.zero, Quaternion.identity, _modelHolder);
            if (_heroAnimationController != null)
            {
                _heroAnimationController.character = this._currentCharacter;
            }
        }

        public void OnSwichPreviousCharacter()
        {
            LeanPool.Despawn(_currentCharacter);
            _currentCharacter = LeanPool.Spawn(_listModelConfig.GetPreviousCharacter(), Vector3.zero, Quaternion.identity, _modelHolder);
            if (_heroAnimationController != null)
            {
                _heroAnimationController.character = this._currentCharacter;
            }
            if (_skillController != null)
            {
                _skillController._currentCharacter = this._currentCharacter;
            }
        }
    }
}
