using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    [CreateAssetMenu(fileName = "ListModelConfig", menuName = "ScriptableObjects/ListModelConfig", order = 2)]
    public class ListModelConfig : ScriptableObject
    {
        [SerializeField]
        private List<Character> _characters = new List<Character>();
        [SerializeField]
        private int currentCharacterIndex;


        public Character GetCurrentCharacter()
        {
            return Dequeue(currentCharacterIndex);
        }


        public Character GetPreviousCharacter()
        {
            if (currentCharacterIndex < 1)
            {
                currentCharacterIndex = _characters.Count;
            }
            return Dequeue(currentCharacterIndex - 1);
        }

        public Character GetNextCharacter()
        {
            if (currentCharacterIndex == _characters.Count - 1)
            {
                currentCharacterIndex = -1;
            }
            return Dequeue(currentCharacterIndex + 1);
        }

        public void Enqueue(Character character)
        {
            _characters.Add(character);
        }


        public Character Dequeue(int index)
        {
            Character character = _characters[index];
            currentCharacterIndex = index;
            return character;
        }
    }
}
