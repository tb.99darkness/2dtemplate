using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    [System.Serializable]
    public struct TurnConfig
    {
        public GameObject enemy;
        public int number;
    }
}
