using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Runtime
{
    [CreateAssetMenu(fileName = "WaveConfig", menuName = "ScriptableObjects/WaveConfig", order = 2)]
    public class WaveConfig : ScriptableObject
    {
        [ShowInInspector]
        public List<TurnConfig> turnConfigs;
        [SerializeField]
        private float _powerLevel = 1;
        [SerializeField]
        private float _delaySpawn = 1;
    }
}
