using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    public static class AnimationConstant
    {
        public static readonly string Is_moving ="is_moving";
        public static readonly string Idle ="Idle";
        public static readonly string Attack_1 ="Attack_1";
        public static readonly string Dead ="Dead";
    }
}
