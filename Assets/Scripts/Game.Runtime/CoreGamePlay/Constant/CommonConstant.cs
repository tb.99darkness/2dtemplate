using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    public static class CommonConstant
    {
        public readonly static Vector3 Vector3Zero = Vector3.zero;
        public readonly static Vector3 Vector3One = Vector3.one;

        public readonly static Vector2 Vector2Zero = Vector2.zero;
        public readonly static Vector2 Vector2One = Vector2.one;
    }
}
