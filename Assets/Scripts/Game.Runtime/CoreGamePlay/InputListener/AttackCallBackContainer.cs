using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    public class MoveCallBackContainer : List<IMoveListener>, IMoveListener
    {
        public void OnCacelMove()
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i].OnCacelMove();
            }
        }

        public void OnMove(Vector2 vector)
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i].OnMove(vector);
            }
        }

        public void OnStartMove()
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i].OnStartMove();
            }
        }
    }
}
