using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    public interface IMoveListener
    {
        void OnStartMove();

        void OnCacelMove();

        void OnMove(Vector2 vector);
    }
}
