using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Runtime
{
    public class AttackCallBackContainer : List<IAttackListener>, IAttackListener
    {
        public void OnAttack()
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i].OnAttack();
            }
        }
    }
}
