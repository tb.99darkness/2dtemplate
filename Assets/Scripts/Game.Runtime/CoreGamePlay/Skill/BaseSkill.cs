using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;

namespace Game.Runtime
{
    [CreateAssetMenu(fileName = "Skill", menuName = "ScriptableObjects/Skill", order = 1)]
    public class BaseSkill : ScriptableObject
    {
        [SerializeField]
        private Bullet _bullet;
        [SerializeField]
        public  int _bulletNumber;
        [SerializeField]
        public float _delayTime;
        public float _lastCast;

        public IEnumerator Excute(Quaternion direct, Vector2 pos, int index, float dmg, LayerMask layerMask)
        {
            yield return new WaitForSeconds(_delayTime * index);
            Bullet bullet = LeanPool.Spawn(_bullet, pos, direct);
            bullet.SetData(dmg, layerMask);
        }

        private void OnEnable()
        {
            _lastCast = 0;
        }
    }
}
