using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;

namespace Game.Runtime
{
    public class Bullet : MonoBehaviour, IUpdateable
    {
        [SerializeField]
        private Collider2D _collider;
        [SerializeField]
        private float _delayDestroy;
        public GameObject _vfx;
        private float _damage;
        private LayerMask _layerMask;

        protected virtual void OnEnable()
        {
            UpdateManager.RegisterFixedUpdate(this);
            if (_delayDestroy != 0)
            {
                StartCoroutine(Despawn());
            }
        }

        public void CustomFixedUpdate()
        {
            Move();
        }

        public void CustomUpdate()
        {
        }

        public virtual void SetData(float damage, LayerMask layerMask)
        {
            _damage = damage;
            _layerMask = layerMask;
        }

        protected virtual void TriggerDamage(HpController hpController)
        {
            hpController.Modifi(-_damage);
            LeanPool.Despawn(gameObject);
            StopAllCoroutines();
            if (_vfx != null)
            {
                GameObject gameObject =  LeanPool.Spawn(_vfx, hpController.transform.position, Quaternion.identity);
                LeanPool.Despawn(gameObject, 0.5f);
            }
        }


        protected virtual void Move()
        {

        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (IsInLayerMask(collision.gameObject, _layerMask))
            {
                HpController hpController;
                if (collision.gameObject.TryGetComponent<HpController>(out hpController))
                {
                    TriggerDamage(hpController);
                }
            }
        }


        public bool IsInLayerMask(GameObject obj, LayerMask layerMask)
        {
            return ((layerMask.value & (1 << obj.layer)) > 0);
        }

        IEnumerator Despawn()
        {
            yield return new WaitForSeconds(_delayDestroy);
            LeanPool.Despawn(gameObject);
        }


        private void OnDisable()
        {
            UpdateManager.UnregisterFixedUpdate(this);
            StopAllCoroutines();
        }
    }
}
