using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;

namespace Game.Runtime
{
    public class LightStrike : Bullet
    {
        [SerializeField]
        private float _spacing;

        protected override void OnEnable()
        {
            base.OnEnable();
            Debug.Log(transform.eulerAngles);
            float direction = transform.eulerAngles.normalized.y;
            if (direction == 0)
            {
                direction = -1;
            }
            transform.position += new Vector3(_spacing * -direction, 0, 0);
        }

        protected override void TriggerDamage(HpController hpController)
        {
            base.TriggerDamage(hpController);
        }


    }
}

