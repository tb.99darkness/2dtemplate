using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;

namespace Game.Runtime
{
    public class Rasegigan : Bullet
    {
        private float _direct;
        protected override void Move()
        {
            _direct = transform.localRotation.normalized.y;
            if (_direct == 0)
            {
                _direct = 1;
            }
            transform.position += (Vector3.right * _direct * 5 * Time.deltaTime);
        }

        protected override void TriggerDamage(HpController hpController)
        {
            base.TriggerDamage(hpController);
        }

    }
}
